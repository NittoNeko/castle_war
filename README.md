# CastleWar

Castle War is a school project made by a group of three people including Yajie Hao, Qiuhan Li and Yehao Wang.
And it was developed on the Android Studio.

Feel free to download the source and build on your own.

# Technical issues

This project is:

  - poorly designed and organized due to lack of time.
  - just to demonstrate our understandings of basic elements of a game.

# Notice

If the build fails or you just want to see what does the game look like,

go to the Youtube link: https://youtu.be/Mif6p0X8Qds

The game starts from 0:39 and ends at 1:03.